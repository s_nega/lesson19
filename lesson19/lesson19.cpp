﻿
#include <iostream>
#include <string>
using namespace std;

class Animal
{
private:
    string sound;

public:

    Animal() {}
    Animal(string _sound) : sound(_sound)
    {}

 
    virtual void Voice()
    {
        cout << "sound" << endl;
    }
};

class Dog : public Animal
{
public:
    Dog() {}
    
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal
{
public:
    Cat(){}
    
    void Voice() override
    {
        cout << "Muau!" << endl;
    }
};

class Cow : public Animal
{
public:
    Cow() {}

    void Voice() override
    {
        cout << "Mooo!" << endl;
    }
};


int main()
{
    int n;
    cout << "how much animals do you need?" << endl;
    cin >> n;
    
    Animal** animalsList = new Animal*[n];

    cout << "select animal: 1->dog, 2->cat, 3->cow" << endl;

    for (int i = 0; i < n; i++)
    {
        int a;
        cin >> a ;

        switch (a)
        {
        case 2:
            animalsList[i] = new Cat();
            cout << "> cat" << endl;
            break;
        case 3:
            animalsList[i] = new Cow();
            cout << "> cow" << endl;
            break;
        default:
            animalsList[i] = new Dog();
            cout << "> dog" << endl;
            break;
        }
    }

    cout << endl;

    for (int i = 0; i < n; i++)
    {
        animalsList[i]->Voice();
        delete animalsList[i];
    }

    delete[] animalsList;
}
